<?php

use Aedart\Validate\BaseValidator;
use \Mockery;
use Faker\Factory as FakerFactory;

/**
 * Class BaseValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\BaseValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class BaseValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    // Failed to actually build a mock of the base validator... due to protected
    // constructor!
//    /**
//     *
//     * Get the base validator mock
//     *
//     * @return Mockery\MockInterface|Aedart\Validate\BaseValidator
//     */
//    protected function getBaseValidatorDummyMock(){
//        //$m = Mockery::mock('DummyValidator')->makePartial();
//        $m = Mockery::mock('DummyValidator')->shouldDeferMissing();
//        //$m = Mockery::mock('DummyValidator');
//        return $m;
//    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::__construct
     * @covers ::isValid
     * @covers ::validate
     */
    public function isValidWithoutValidateOptions(){
        $value = $this->faker->address;

        $this->assertTrue(DummyValidator::isValid($value), sprintf('Should had been true for value %s (faker generated))', $value));
    }

    /**
     * NB: This test is more about making sure that populate actually
     * invokes the __set() method
     *
     * @test
     * @covers ::__construct
     * @covers ::isValid
     * @covers ::populate
     * @covers ::validate
     */
    public function isValidWithValidateOptions(){
        // These options have no getters / setters, thus
        // they should trigger an exception
        $options = [
            'my'        => $this->faker->word,
            'custom'    => $this->faker->century,
        ];

        $value = $this->faker->address;

        $this->assertTrue(DummyValidator::isValid($value), sprintf('Should had been true for value %s (faker generated) and options %s)', $value, print_r($options, true)));
    }

    /**
     * NB: This test is more about making sure that populate actually
     * invokes the __set() method - and trigger an exception
     *
     * @test
     * @covers ::__construct
     * @covers ::isValid
     * @covers ::populate
     * @covers ::validate
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function isValidWithInvalidValidateOptions(){
        // These options have no getters / setters, thus
        // they should trigger an exception
        $options = [
            'my'        => $this->faker->word,
            'custom'    => $this->faker->century,
            'options'   => $this->faker->words(3)
        ];

        $value = $this->faker->address;

        DummyValidator::isValid($value, $options);
    }

    /**
     * @test
     * @covers ::getLastErrorMessage
     */
    public function getLastErrorMessage(){
        DummyValidatorWithLastErrorMsg::isValid(false);
        $this->assertNotEmpty(DummyValidatorWithLastErrorMsg::getLastErrorMessage());

        DummyValidatorWithLastErrorMsg::isValid(true);
        $this->assertEmpty(DummyValidatorWithLastErrorMsg::getLastErrorMessage());
    }
}

class DummyValidator extends BaseValidator {

    protected $my = null;
    protected  $custom = null;

    protected function setMy($v){
        $this->my = $v;
    }

    protected function setCustom($v){
        $this->custom = $v;
    }

    public function validate() {
        return true;
    }
}

class DummyValidatorWithLastErrorMsg extends BaseValidator{

    /**
     * Validate the set validate-value, based upon eventual set validate-
     * options (if supported by concrete validator)
     *
     * @return bool True if the value is valid, false if not
     */
    public function validate() {
        $value = $this->getValidateValue();
        if($value === true){
            return true;
        }

        self::$lastErrorMessage = 'my custom last error msg';
        return false;
    }
}