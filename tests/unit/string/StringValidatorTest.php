<?php

use Aedart\Validate\StringValidator;

/**
 * Class StringValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\StringValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class StringValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values to be tested are inspired from http://php.net/manual/en/function.is-numeric.php, example #1
             */
            ["", [], true],
            ["42", [], true],
            [1337, [], false],
            [0x539, [], false],
            [02471, [], false],
            [1337e0, [], false],
            ['not numeric', [], true],
            [array(), [], false],
            [9.1, [], false],

            /*
             * With options
             */
            ["42", [StringValidator::MIN_RANGE => 0], true],
            ["wee", [StringValidator::MIN_RANGE => 3], true],
            ["wee", [StringValidator::MIN_RANGE => 3, StringValidator::INCLUSIVE_RANGE => false], false],
            ["no way", [StringValidator::MAX_RANGE => 6], true],
            ["no way", [StringValidator::MAX_RANGE => 6, StringValidator::INCLUSIVE_RANGE => false], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMinRange
     * @covers ::getDefaultMaxRange
     * @covers ::isMinRangeValid
     * @covers ::isMaxRangeValid
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = StringValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }
}