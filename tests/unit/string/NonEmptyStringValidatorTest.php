<?php

use Aedart\Validate\String\NonEmptyStringValidator;

/**
 * Class NonEmptyStringValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\String\NonEmptyStringValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class NonEmptyStringValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values to be tested are inspired from http://php.net/manual/en/function.is-numeric.php, example #1
             */
            ["", [], false],

            /*
             * With options
             */
            ["no way", [NonEmptyStringValidator::MAX_RANGE => 6], true],
            ["no way", [NonEmptyStringValidator::MAX_RANGE => 6, NonEmptyStringValidator::INCLUSIVE_RANGE => false], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMinRange
     * @covers ::getDefaultMaxRange
     * @covers ::isMinRangeValid
     * @covers ::isMaxRangeValid
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = NonEmptyStringValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }
}