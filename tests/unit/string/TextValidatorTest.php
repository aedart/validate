<?php

use Aedart\Validate\String\TextValidator;
use Faker\Factory as FakerFactory;

/**
 * Class TextValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\String\TextValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class TextValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        $faker = FakerFactory::create();

        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values to be tested are inspired from http://php.net/manual/en/function.is-numeric.php, example #1
             */
            ["", [], true],
            [$faker->name, [], true],
            [$faker->paragraph(rand(20, 250)), [], true],
            [$faker->text(rand(250, 2000)), [], true],

            /*
             * With options
             */
            ["no way", [TextValidator::MAX_RANGE => 6], true],
            ["no way", [TextValidator::MAX_RANGE => 6, TextValidator::INCLUSIVE_RANGE => false], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMinRange
     * @covers ::getDefaultMaxRange
     * @covers ::isMinRangeValid
     * @covers ::isMaxRangeValid
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = TextValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}