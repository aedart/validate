<?php

use Aedart\Validate\Helper\FloatPrecisionRangeComparator;

/**
 * Class FloatPrecisionRangeComparator
 *
 * @coversDefaultClass Aedart\Validate\Helper\FloatPrecisionRangeComparator
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class FloatPrecisionRangeComparatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            // $value, $min, $max, $inclusive, $expectedResult
            [10.0, 0, 10, true, true],
            [10.12, 0, 10, false, true],
            [1.0, 0, 0, true, true],
            [1.1, 0, 1, true, true],
            [-10.0, 0, 0, false, false],
            [-10.1, 0, -10, false, false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isWithinRange
     * @covers ::countNumberPrecision
     *
     * @param int $value
     * @param int $min
     * @param int $max
     * @param bool $inclusive
     * @param bool $expectedResult
     */
    public function isWithinRange($value, $min, $max, $inclusive, $expectedResult){
        $result = FloatPrecisionRangeComparator::isWithinRange($value, $min, $max, $inclusive);

        $isInclusive = 'true';
        if(!$inclusive){
            $isInclusive = 'no';
        }

        $failMsg = sprintf('Failed validating that %d is greater than %d (min), less than %d (max), inclusive check (%s)',
            $value,
            $min,
            $max,
            $isInclusive
        );

        $this->assertSame($expectedResult, $result, $failMsg);
    }



}