<?php

use Aedart\Validate\Helper\RangeComparator;

/**
 * Class RangeComparatorTest
 *
 * @coversDefaultClass Aedart\Validate\Helper\RangeComparator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class RangeComparatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            // $unitLength, $min, $max, $inclusive, $expectedResult
            [10, 0, 10, true, true],
            [10, 0, 10, false, false],
            [1, 0, 0, true, false],
            [1, 0, 1, true, true],
            [-10, -10, 0, true, true],
            [-10, -10, 0, false, false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isWithinRange
     *
     * @param int $unitLength
     * @param int $min
     * @param int $max
     * @param bool $inclusive
     * @param bool $expectedResult
     */
    public function isWithinRange($unitLength, $min, $max, $inclusive, $expectedResult){
        $result = RangeComparator::isWithinRange($unitLength, $min, $max, $inclusive);

        $isInclusive = 'true';
        if(!$inclusive){
            $isInclusive = 'no';
        }

        $failMsg = sprintf('Failed validating that %d is greater than %d (min), less than %d (max), inclusive check (%s)',
            $unitLength,
            $min,
            $max,
            $isInclusive
        );

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}