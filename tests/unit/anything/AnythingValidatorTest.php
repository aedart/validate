<?php
use Aedart\Validate\AnythingValidator;
use \Mockery;
use Faker\Factory as FakerFactory;

/**
 * Class AnythingValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\AnythingValidator
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class AnythingValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
        Mockery::close();
    }

    // =======================================================================//
    //   Actual tests
    // =======================================================================//

    /**
     * @test
     * @covers  ::isValid
     * @covers  ::getLastErrorMessage
     */
    public function testIsValid()
    {
        $value = $this->faker->creditCardDetails;

        $this->assertTrue(AnythingValidator::isValid($value), sprintf('Should had been true for value %s (faker generated))', var_dump($value)));
        $this->assertEmpty(AnythingValidator::getLastErrorMessage());
    }


}