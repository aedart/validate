<?php

use Aedart\Validate\Filesystem\DirectoryValidator;
use Codeception\Configuration;

/**
 * Class DirectoryValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\Filesystem\DirectoryValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class DirectoryValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Invalid results
             */
            ["", [], false],
            ["42", [], false],
            [1337, [], false],
            [0x539, [], false],
            [02471, [], false],
            [1337e0, [], false],
            ['not numeric', [], false],
            [array(), [], false],
            [9.1, [], false],

            /*
             * Valid results
             */
            [Configuration::dataDir(), [], true],
            [Configuration::outputDir(), [], true],
            [Configuration::helpersDir(), [], true],

            /*
             * This validator doesn't support options
             */
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = DirectoryValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}