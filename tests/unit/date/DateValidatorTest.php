<?php
use Aedart\Validate\Date\DateValidator;
use \Mockery;
use Faker\Factory as FakerFactory;

/**
 * Class DateValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\Date\DateValidator
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class DateValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
        Mockery::close();
    }

    // =======================================================================//
    //   Actual tests
    // =======================================================================//

    /**
     * @test
     * @covers  ::validate
     */
    public function testIsValid()
    {
        $value = $this->faker->dateTime;

        $this->assertTrue(DateValidator::isValid($value), sprintf('Should had been true for value %s (faker generated))', var_dump($value,1)));
    }

    /**
     * @test
     * @covers  ::validate
     */
    public function testIsInvalid()
    {
        $date = $this->faker->dayOfWeek;

        $this->assertFalse(DateValidator::isValid($date),'Date is valid');
    }


}