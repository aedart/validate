<?php
use Aedart\Validate\Traits\MinPrecisionRangeTrait;

/**
 * Class MinPrecisionRangeTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\MinPrecisionRangeTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class MinPrecisionRangeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return DummyMinPrecisionRangeContainer
     */
    protected function getContainerDummy(){
        return new DummyMinPrecisionRangeContainer();
    }

    /**
     * @return DummyMinPrecisionRangeContainerWithDefault
     */
    protected function getContainerDummyWithDefault(){
        return new DummyMinPrecisionRangeContainerWithDefault();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultMinPrecisionRange
     * @covers ::getDefaultMinPrecisionRange
     */
    public function getDefaultMinPrecisionRange(){
        $dummy = $this->getContainerDummy();

        $this->assertFalse($dummy->hasDefaultMinPrecisionRange());
        $this->assertNull($dummy->getDefaultMinPrecisionRange());
    }

    /**
     * @test
     * @covers ::hasMinPrecisionRange
     */
    public function hasNoMinPrecisionRange(){
        $dummy = $this->getContainerDummy();
        $this->assertFalse($dummy->hasMinPrecisionRange());
    }

    /**
     * @test
     * @covers ::setMinPrecisionRange
     * @covers ::getMinPrecisionRange
     * @covers ::hasMinPrecisionRange
     * @covers ::isMinPrecisionRangeValid
     */
    public function setAndGetMinPrecisionRange(){
        $dummy = $this->getContainerDummy();

        $minRange = 123;

        $dummy->setMinPrecisionRange($minRange);

        $this->assertTrue($dummy->hasMinPrecisionRange());
        $this->assertSame($minRange, $dummy->getMinPrecisionRange());
    }

    /**
     * @test
     * @covers ::setMinPrecisionRange
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function attemptSetInvalidMinPrecisionRange(){
        $dummy = $this->getContainerDummy();

        $minRange = 'weee';

        $dummy->setMinPrecisionRange($minRange);
    }

    /**
     * @test
     * @covers ::setMinPrecisionRange
     * @covers ::getMinPrecisionRange
     * @covers ::hasMinPrecisionRange
     * @covers ::hasDefaultMinPrecisionRange
     * @covers ::getDefaultMinPrecisionRange
     * @covers ::isMinPrecisionRangeValid
     */
    public function getCustomDefaultMinPrecisionRange(){
        $dummy = $this->getContainerDummyWithDefault();

        $this->assertTrue($dummy->hasDefaultMinPrecisionRange());
        $this->assertSame(456, $dummy->getMinPrecisionRange());
    }

}


class DummyMinPrecisionRange {
    use MinPrecisionRangeTrait;
}

class DummyMinPrecisionRangeContainer extends DummyMinPrecisionRange{

    public function setMinPrecisionRange($value) {
        parent::setMinPrecisionRange($value);
    }

    public function getMinPrecisionRange() {
        return parent::getMinPrecisionRange();
    }

    public function hasMinPrecisionRange() {
        return parent::hasMinPrecisionRange();
    }

    public function getDefaultMinPrecisionRange() {
        return parent::getDefaultMinPrecisionRange();
    }

    public function hasDefaultMinPrecisionRange() {
        return parent::hasDefaultMinPrecisionRange();
    }

    public function isMinPrecisionRangeValid($value) {
        return parent::isMinPrecisionRangeValid($value);
    }

}

class DummyMinPrecisionRangeContainerWithDefault extends DummyMinPrecisionRangeContainer {
    public function getDefaultMinPrecisionRange() {
        return 456;
    }
}