<?php

use Aedart\Validate\Traits\MaxRangeTrait;

/**
 * Class MaxRangeTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\MaxRangeTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class MaxRangeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return DummyMaxRangeContainer
     */
    protected function getContainerDummy(){
        return new DummyMaxRangeContainer();
    }

    /**
     * @return DummyMaxRangeContainerWithDefault
     */
    protected function getContainerDummyWithDefault(){
        return new DummyMaxRangeContainerWithDefault();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultMaxRange
     * @covers ::getDefaultMaxRange
     */
    public function getDefaultMaxRange(){
        $dummy = $this->getContainerDummy();

        $this->assertFalse($dummy->hasDefaultMaxRange());
        $this->assertNull($dummy->getDefaultMaxRange());
    }

    /**
     * @test
     * @covers ::hasMaxRange
     */
    public function hasNoMaxRange(){
        $dummy = $this->getContainerDummy();
        $this->assertFalse($dummy->hasMaxRange());
    }

    /**
     * @test
     * @covers ::setMaxRange
     * @covers ::getMaxRange
     * @covers ::hasMaxRange
     * @covers ::isMaxRangeValid
     */
    public function setAndGetMinRange(){
        $dummy = $this->getContainerDummy();

        $maxRange = 123;

        $dummy->setMaxRange($maxRange);

        $this->assertTrue($dummy->hasMaxRange());
        $this->assertSame($maxRange, $dummy->getMaxRange());
    }

    /**
     * @test
     * @covers ::setMaxRange
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function attemptSetInvalidMinRange(){
        $dummy = $this->getContainerDummy();

        $minRange = 'weee';

        $dummy->setMaxRange($minRange);
    }

    /**
     * @test
     * @covers ::setMaxRange
     * @covers ::getMaxRange
     * @covers ::hasMaxRange
     * @covers ::hasDefaultMaxRange
     * @covers ::getDefaultMaxRange
     * @covers ::isMaxRangeValid
     */
    public function getCustomDefaultMinRange(){
        $dummy = $this->getContainerDummyWithDefault();

        $this->assertTrue($dummy->hasDefaultMaxRange());
        $this->assertSame(456, $dummy->getMaxRange());
    }
}

class DummyMaxRange {
    use MaxRangeTrait;
}

class DummyMaxRangeContainer extends DummyMaxRange{

    public function setMaxRange($value) {
        parent::setMaxRange($value);
    }

    public function getMaxRange() {
        return parent::getMaxRange();
    }

    public function hasMaxRange() {
        return parent::hasMaxRange();
    }

    public function getDefaultMaxRange() {
        return parent::getDefaultMaxRange();
    }

    public function hasDefaultMaxRange() {
        return parent::hasDefaultMaxRange();
    }

    public function isMaxRangeValid($value) {
        return parent::isMaxRangeValid($value);
    }

}

class DummyMaxRangeContainerWithDefault extends DummyMaxRangeContainer {
    public function getDefaultMaxRange() {
        return 456;
    }
}