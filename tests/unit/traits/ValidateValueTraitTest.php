<?php

use Aedart\Validate\Interfaces\ValidateValueAware;
use Aedart\Validate\Traits\ValidateValueTrait;
use Faker\Factory as FakerFactory;

/**
 * Class ValidateValueTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\ValidateValueTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ValidateValueTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Faker
     *
     * @var \Faker\Generator
     */
    protected $faker = null;

    protected function _before()
    {
        $this->faker = FakerFactory::create();
    }

    protected function _after()
    {
    }


    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Validate\Interfaces\ValidateValueAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Validate\Traits\ValidateValueTrait');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::setValidateValue
     * @covers ::getValidateValue
     */
    public function getAndSetValidateValue(){
        $trait = $this->getTraitMock();

        $value = $this->faker->word;

        $trait->setValidateValue($value);

        $this->assertSame($value, $trait->getValidateValue());

    }

}