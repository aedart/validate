<?php

use Aedart\Validate\Traits\InclusiveRangeTrait;

/**
 * Class InclusiveRangeTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\InclusiveRangeTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class InclusiveRangeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return DummyInclusiveRangeContainer
     */
    protected function getContainerDummy(){
        return new DummyInclusiveRangeContainer();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultInclusiveRange
     * @covers ::getInclusiveRange
     * @covers ::getExclusiveRange
     * @covers ::hasInclusiveRange
     * @covers ::setInclusiveRange
     * @covers ::isInclusiveRangeValid
     * @covers ::getDefaultInclusiveRange
     * @covers ::isInclusiveRange
     * @covers ::isExclusive
     */
    public function getDefaultInclusiveRange(){
        $dummy = $this->getContainerDummy();

        $this->assertTrue($dummy->hasDefaultInclusiveRange(), 'Should have a default specified');
        $this->assertTrue($dummy->getInclusiveRange(), 'Default should be true');
        $this->assertTrue($dummy->isInclusiveRange(), 'Should be inclusive range');
        $this->assertFalse($dummy->isExclusive(), 'Should not be exclusive');
    }

    /**
     * @test
     * @covers ::setExclusiveRange
     * @covers ::setInclusiveRange
     * @covers ::isInclusiveRange
     * @covers ::isExclusive
     */
    public function setAndGetInclusiveRange(){
        $dummy = $this->getContainerDummy();

        //$dummy->setInclusiveRange(false);
        $dummy->setExclusiveRange(true);

        $this->assertFalse($dummy->isInclusiveRange(), 'Should not be inclusive');
        $this->asserttrue($dummy->isExclusive(), 'Should be exclusive');
    }

    /**
     * @test
     * @covers ::setInclusiveRange
     * @covers ::isInclusiveRangeValid
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function attemptSetInvalidInclusiveRange(){
        $dummy = $this->getContainerDummy();

        $dummy->setInclusiveRange('weeee');
    }
}

class DummyInclusiveRange {
    use InclusiveRangeTrait;
}

class DummyInclusiveRangeContainer extends DummyInclusiveRange{
    public function setInclusiveRange($value) {
        parent::setInclusiveRange($value);
    }

    public function setExclusiveRange($value) {
        parent::setExclusiveRange($value);
    }

    public function getInclusiveRange() {
        return parent::getInclusiveRange();
    }

    public function getExclusiveRange() {
        return parent::getExclusiveRange();
    }

    public function isInclusiveRange() {
        return parent::isInclusiveRange();
    }

    public function isExclusive() {
        return parent::isExclusive();
    }

    public function hasInclusiveRange() {
        return parent::hasInclusiveRange();
    }

    public function getDefaultInclusiveRange() {
        return parent::getDefaultInclusiveRange();
    }

    public function hasDefaultInclusiveRange() {
        return parent::hasDefaultInclusiveRange();
    }

    public function isInclusiveRangeValid($value) {
        return parent::isInclusiveRangeValid($value);
    }
}