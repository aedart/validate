<?php

use Aedart\Validate\Traits\MinRangeTrait;

/**
 * Class MinRangeTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\MinRangeTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class MinRangeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return DummyMinRangeContainer
     */
    protected function getContainerDummy(){
        return new DummyMinRangeContainer();
    }

    /**
     * @return DummyMinRangeContainerWithDefault
     */
    protected function getContainerDummyWithDefault(){
        return new DummyMinRangeContainerWithDefault();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultMinRange
     * @covers ::getDefaultMinRange
     */
    public function getDefaultMinRange(){
        $dummy = $this->getContainerDummy();

        $this->assertFalse($dummy->hasDefaultMinRange());
        $this->assertNull($dummy->getDefaultMinRange());
    }

    /**
     * @test
     * @covers ::hasMinRange
     */
    public function hasNoMinRange(){
        $dummy = $this->getContainerDummy();
        $this->assertFalse($dummy->hasMinRange());
    }

    /**
     * @test
     * @covers ::setMinRange
     * @covers ::getMinRange
     * @covers ::hasMinRange
     * @covers ::isMinRangeValid
     */
    public function setAndGetMinRange(){
        $dummy = $this->getContainerDummy();

        $minRange = 123;

        $dummy->setMinRange($minRange);

        $this->assertTrue($dummy->hasMinRange());
        $this->assertSame($minRange, $dummy->getMinRange());
    }

    /**
     * @test
     * @covers ::setMinRange
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function attemptSetInvalidMinRange(){
        $dummy = $this->getContainerDummy();

        $minRange = 'weee';

        $dummy->setMinRange($minRange);
    }

    /**
     * @test
     * @covers ::setMinRange
     * @covers ::getMinRange
     * @covers ::hasMinRange
     * @covers ::hasDefaultMinRange
     * @covers ::getDefaultMinRange
     * @covers ::isMinRangeValid
     */
    public function getCustomDefaultMinRange(){
        $dummy = $this->getContainerDummyWithDefault();

        $this->assertTrue($dummy->hasDefaultMinRange());
        $this->assertSame(123, $dummy->getMinRange());
    }
}

class DummyMinRange {
    use MinRangeTrait;
}

class DummyMinRangeContainer extends DummyMinRange{

    public function setMinRange($value) {
        parent::setMinRange($value);
    }

    public function getMinRange() {
        return parent::getMinRange();
    }

    public function hasMinRange() {
        return parent::hasMinRange();
    }

    public function getDefaultMinRange() {
        return parent::getDefaultMinRange();
    }

    public function hasDefaultMinRange() {
        return parent::hasDefaultMinRange();
    }

    public function isMinRangeValid($value) {
        return parent::isMinRangeValid($value);
    }
}

class DummyMinRangeContainerWithDefault extends DummyMinRangeContainer {
    public function getDefaultMinRange() {
        return 123;
    }
}