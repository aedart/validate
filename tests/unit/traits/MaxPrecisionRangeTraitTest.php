<?php
use Aedart\Validate\Traits\MaxPrecisionRangeTrait;

/**
 * Class MaxPrecisionRangeTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\MaxPrecisionRangeTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class MaxPrecisionRangeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return DummyMaxPrecisionRangeContainer
     */
    protected function getContainerDummy(){
        return new DummyMaxPrecisionRangeContainer();
    }

    /**
     * @return DummyMaxPrecisionRangeContainerWithDefault
     */
    protected function getContainerDummyWithDefault(){
        return new DummyMaxPrecisionRangeContainerWithDefault();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultMaxPrecisionRange
     * @covers ::getDefaultMaxPrecisionRange
     */
    public function getDefaultMaxPrecisionRange(){
        $dummy = $this->getContainerDummy();

        $this->assertFalse($dummy->hasDefaultMaxPrecisionRange());
        $this->assertNull($dummy->getDefaultMaxPrecisionRange());
    }

    /**
     * @test
     * @covers ::hasMaxPrecisionRange
     */
    public function hasNoMaxPrecisionRange(){
        $dummy = $this->getContainerDummy();
        $this->assertFalse($dummy->hasMaxPrecisionRange());
    }

    /**
     * @test
     * @covers ::setMaxPrecisionRange
     * @covers ::getMaxPrecisionRange
     * @covers ::hasMaxPrecisionRange
     * @covers ::isMaxPrecisionRangeValid
     */
    public function setAndGetMaxPrecisionRange(){
        $dummy = $this->getContainerDummy();

        $maxRange = 123;

        $dummy->setMaxPrecisionRange($maxRange);

        $this->assertTrue($dummy->hasMaxPrecisionRange());
        $this->assertSame($maxRange, $dummy->getMaxPrecisionRange());
    }

    /**
     * @test
     * @covers ::setMaxPrecisionRange
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function attemptSetInvalidMaxPrecisionRange(){
        $dummy = $this->getContainerDummy();

        $maxRange = 'weee';

        $dummy->setMaxPrecisionRange($maxRange);
    }

    /**
     * @test
     * @covers ::setMaxPrecisionRange
     * @covers ::getMaxPrecisionRange
     * @covers ::hasMaxPrecisionRange
     * @covers ::hasDefaultMaxPrecisionRange
     * @covers ::getDefaultMaxPrecisionRange
     * @covers ::isMaxPrecisionRangeValid
     */
    public function getCustomDefaultMaxPrecisionRange(){
        $dummy = $this->getContainerDummyWithDefault();

        $this->assertTrue($dummy->hasDefaultMaxPrecisionRange());
        $this->assertSame(456, $dummy->getMaxPrecisionRange());
    }

}


class DummyMaxPrecisionRange {
    use MaxPrecisionRangeTrait;
}

class DummyMaxPrecisionRangeContainer extends DummyMaxPrecisionRange{

    public function setMaxPrecisionRange($value) {
        parent::setMaxPrecisionRange($value);
    }

    public function getMaxPrecisionRange() {
        return parent::getMaxPrecisionRange();
    }

    public function hasMaxPrecisionRange() {
        return parent::hasMaxPrecisionRange();
    }

    public function getDefaultMaxPrecisionRange() {
        return parent::getDefaultMaxPrecisionRange();
    }

    public function hasDefaultMaxPrecisionRange() {
        return parent::hasDefaultMaxPrecisionRange();
    }

    public function isMaxPrecisionRangeValid($value) {
        return parent::isMaxPrecisionRangeValid($value);
    }

}

class DummyMaxPrecisionRangeContainerWithDefault extends DummyMaxPrecisionRangeContainer {
    public function getDefaultMaxPrecisionRange() {
        return 456;
    }
}