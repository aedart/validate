<?php
use Aedart\Validate\Traits\InclusivePrecisionRangeTrait;

/**
 * Class InclusivePrecisionRangeTraitTest
 *
 * @coversDefaultClass Aedart\Validate\Traits\InclusivePrecisionRangeTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class InclusivePrecisionRangeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * @return DummyInclusivePrecisionRangeContainer
     */
    protected function getContainerDummy(){
        return new DummyInclusivePrecisionRangeContainer();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasDefaultInclusivePrecisionRange
     * @covers ::getInclusivePrecisionRange
     * @covers ::getExclusivePrecisionRange
     * @covers ::hasInclusivePrecisionRange
     * @covers ::setInclusivePrecisionRange
     * @covers ::isInclusivePrecisionRangeValid
     * @covers ::getDefaultInclusivePrecisionRange
     * @covers ::isInclusivePrecisionRange
     * @covers ::isExclusive
     */
    public function getDefaultInclusivePrecisionRange(){
        $dummy = $this->getContainerDummy();

        $this->assertTrue($dummy->hasDefaultInclusivePrecisionRange(), 'Should have a default specified');
        $this->assertTrue($dummy->getInclusivePrecisionRange(), 'Default should be true');
        $this->assertTrue($dummy->isInclusivePrecisionRange(), 'Should be inclusive precision range');
        $this->assertFalse($dummy->isExclusive(), 'Should not be exclusive');
    }

    /**
     * @test
     * @covers ::setExclusivePrecisionRange
     * @covers ::setInclusivePrecisionRange
     * @covers ::isInclusivePrecisionRange
     * @covers ::isExclusive
     */
    public function setAndGetInclusivePrecisionRange(){
        $dummy = $this->getContainerDummy();

        $dummy->setExclusivePrecisionRange(true);

        $this->assertFalse($dummy->isInclusivePrecisionRange(), 'Should not be inclusive');
        $this->asserttrue($dummy->isExclusive(), 'Should be exclusive');
    }

    /**
     * @test
     * @covers ::setInclusivePrecisionRange
     * @covers ::isInclusivePrecisionRangeValid
     *
     * @expectedException \Aedart\Validate\Exception\InvalidValidateOptionException
     */
    public function attemptSetInvalidInclusivePrecisionRange(){
        $dummy = $this->getContainerDummy();

        $dummy->setInclusivePrecisionRange('weeee');
    }

}

class DummyInclusivePrecisionRange {
    use InclusivePrecisionRangeTrait;
}


class DummyInclusivePrecisionRangeContainer extends DummyInclusivePrecisionRange{
    public function setInclusivePrecisionRange($value) {
        parent::setInclusivePrecisionRange($value);
    }

    public function setExclusivePrecisionRange($value) {
        parent::setExclusivePrecisionRange($value);
    }

    public function getInclusivePrecisionRange() {
        return parent::getInclusivePrecisionRange();
    }

    public function getExclusivePrecisionRange() {
        return parent::getExclusivePrecisionRange();
    }

    public function isInclusivePrecisionRange() {
        return parent::isInclusivePrecisionRange();
    }

    public function isExclusive() {
        return parent::isExclusive();
    }

    public function hasInclusivePrecisionRange() {
        return parent::hasInclusivePrecisionRange();
    }

    public function getDefaultInclusivePrecisionRange() {
        return parent::getDefaultInclusivePrecisionRange();
    }

    public function hasDefaultInclusivePrecisionRange() {
        return parent::hasDefaultInclusivePrecisionRange();
    }

    public function isInclusivePrecisionRangeValid($value) {
        return parent::isInclusivePrecisionRangeValid($value);
    }
}