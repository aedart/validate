<?php

use Aedart\Validate\Number\FloatValidator;
/**
 * Class FloatValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\Number\FloatValidator
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class FloatValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values without options
             */
            [23.00, [], true],
            [-23, [], false],
            ['23', [], false],
            [23.5, [], true],
            [null, [], false],
            [true, [], false],
            [false, [], false],
            [array(), [], false],

            /*
             * With options
             */
            [1.1, [FloatValidator::MIN_PRECISION_RANGE => 1], true],
            [-1.000, [FloatValidator::MIN_RANGE => -1], true],
            [1.0, [FloatValidator::MIN_RANGE => 1, FloatValidator::INCLUSIVE_RANGE => false], false],
            [-1.0, [FloatValidator::MIN_RANGE => -1, FloatValidator::INCLUSIVE_RANGE => false], false],
            [1.0, [FloatValidator::MAX_RANGE => 1], true],
            [-1.0, [FloatValidator::MAX_RANGE => -1], true],
            [1.0, [FloatValidator::MAX_RANGE => 1, FloatValidator::INCLUSIVE_RANGE => false], false],
            [1.0, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 10, FloatValidator::INCLUSIVE_RANGE => false], false],
            [5.0, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 10, FloatValidator::INCLUSIVE_RANGE => false], true],
            [10.0, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 10, FloatValidator::INCLUSIVE_RANGE => false], false],
            [10.0, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 10, FloatValidator::INCLUSIVE_RANGE => false], false],
            [10.10, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 10, FloatValidator::INCLUSIVE_RANGE => false, FloatValidator::MIN_PRECISION_RANGE=>1], false],
            [10.10, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 100, FloatValidator::INCLUSIVE_RANGE => false, FloatValidator::MIN_PRECISION_RANGE=>1], true],
            [10.10, [FloatValidator::MIN_RANGE => 1, FloatValidator::MAX_RANGE => 100, FloatValidator::INCLUSIVE_RANGE => false, FloatValidator::MIN_PRECISION_RANGE=>1, FloatValidator::MAX_PRECISION_RANGE => 2], true],
            [10.123232, [FloatValidator::MIN_PRECISION_RANGE => 5, FloatValidator::INCLUSIVE_PRECISION_RANGE=>false ], true],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMinRange
     * @covers ::getDefaultMinPrecisionRange
     * @covers ::getDefaultMaxRange
     * @covers ::getDefaultMaxPrecisionRange
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){

        // Testing via 'Alias' class
        $result = FloatValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }





}