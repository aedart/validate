<?php

use Aedart\Validate\Number\Integer\UnsignedIntegerValidator;

/**
 * Class UnsignedIntegerValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\Number\Integer\UnsignedIntegerValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class UnsignedIntegerValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values without options
             */
            [0, [], true],
            [23, [], true],
            [-1, [], false],
            ['23', [], false],
            [23.5, [], false],
            ['23.5', [], false],
            [null, [], false],
            [true, [], false],
            [false, [], false],
            [array(), [], false],

            /*
             * With options
             */
            [1, [UnsignedIntegerValidator::MAX_RANGE => 1], true],
            [-1, [UnsignedIntegerValidator::MAX_RANGE => -1], false],
            [10, [UnsignedIntegerValidator::MAX_RANGE => 10], true],
            [10, [UnsignedIntegerValidator::MAX_RANGE => 10, UnsignedIntegerValidator::INCLUSIVE_RANGE => false], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMaxRange
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = UnsignedIntegerValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}