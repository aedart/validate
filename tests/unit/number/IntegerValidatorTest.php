<?php

use Aedart\Validate\Number\IntegerValidator;
use Aedart\Validate\Number\Integer\SignedIntegerValidator;

/**
 * Class IntegerValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\Number\IntegerValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class IntegerValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values without options
             */
            [23, [], true],
            [-23, [], true],
            ['23', [], false],
            [23.5, [], false],
            ['23.5', [], false],
            [null, [], false],
            [true, [], false],
            [false, [], false],
            [array(), [], false],

            /*
             * With options
             */
            [1, [IntegerValidator::MIN_RANGE => 1], true],
            [-1, [IntegerValidator::MIN_RANGE => -1], true],
            [1, [IntegerValidator::MIN_RANGE => 1, IntegerValidator::INCLUSIVE_RANGE => false], false],
            [-1, [IntegerValidator::MIN_RANGE => -1, IntegerValidator::INCLUSIVE_RANGE => false], false],
            [1, [IntegerValidator::MAX_RANGE => 1], true],
            [-1, [IntegerValidator::MAX_RANGE => -1], true],
            [1, [IntegerValidator::MAX_RANGE => 1, IntegerValidator::INCLUSIVE_RANGE => false], false],
            [1, [IntegerValidator::MIN_RANGE => 1, IntegerValidator::MAX_RANGE => 10, IntegerValidator::INCLUSIVE_RANGE => false], false],
            [5, [IntegerValidator::MIN_RANGE => 1, IntegerValidator::MAX_RANGE => 10, IntegerValidator::INCLUSIVE_RANGE => false], true],
            [10, [IntegerValidator::MIN_RANGE => 1, IntegerValidator::MAX_RANGE => 10, IntegerValidator::INCLUSIVE_RANGE => false], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMinRange
     * @covers ::getDefaultMaxRange
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        //$result = IntegerValidator::isValid($value, $validateOptions);

        // Testing via 'Alias' class
        $result = SignedIntegerValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}