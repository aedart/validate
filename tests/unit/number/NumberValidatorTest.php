<?php

use Aedart\Validate\NumberValidator;

/**
 * Class NumberValidatorTest
 *
 * @coversDefaultClass Aedart\Validate\NumberValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class NumberValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(){
        return [
            /*
             * Syntax: $value, $validateOptions, $expectedResult
             */

            /*
             * Values to be tested are inspired from http://php.net/manual/en/function.is-numeric.php, example #1
             */
            ["42", [], true],
            [1337, [], true],
            [0x539, [], true],
            [02471, [], true],
            [0b10100111001, [], true],
            [1337e0, [], true],
            ['not numeric', [], false],
            [array(), [], false],
            [9.1, [], true],

            /*
             * With options
             */
            ["42", [NumberValidator::MIN_RANGE => 0], true],
            ["42", [NumberValidator::MIN_RANGE => 45], false],
            [10, [NumberValidator::MAX_RANGE => 10], true],
            [10, [NumberValidator::MAX_RANGE => 10, NumberValidator::INCLUSIVE_RANGE => false], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     *
     * @dataProvider dataProvider
     *
     * @covers ::isValid
     * @covers ::validate
     * @covers ::getDefaultMinRange
     * @covers ::getDefaultMaxRange
     * @covers ::isMinRangeValid
     * @covers ::isMaxRangeValid
     *
     * @param mixed $value
     * @param array $validateOptions
     * @param bool $expectedResult
     */
    public function isValid($value, $validateOptions, $expectedResult){
        $result = NumberValidator::isValid($value, $validateOptions);

        $failMsg = sprintf('Failed validating that %s is valid, with the following options %s', var_export($value, true), var_export($validateOptions, true));

        $this->assertSame($expectedResult, $result, $failMsg);
    }

}