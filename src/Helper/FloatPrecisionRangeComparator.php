<?php namespace Aedart\Validate\Helper;

/**
 * Class FloatPrecisionRangeComparator
 *
 * Precision range comparator, can validate if a given float value precision length is
 * within a specified range (inclusive or exclusive)
 *
 * @package Aedart\Validate\Helper
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class FloatPrecisionRangeComparator
{

    /**
     * Check if the given value float precision is within the specified range
     *
     * @param mixed $value  Float Value to be checked if it is within the specified number precision range
     * @param int $minimumPrecisionRange The minimum precision range
     * @param int $maximumPrecisionRange The maximum precision range
     * @param bool $inclusive [Optional][Default true] If true, precision range is checked inclusively, false if range
     *                          is to be checked exclusively
     * @return bool True if the given length is within the specified precision range, false if not
     */
    public static function isWithinRange($value, $minimumPrecisionRange, $maximumPrecisionRange, $inclusive = true)
    {
        // count precision numbers from the value
        $lengthToBeChecked = self::countNumberPrecision($value);
        // Inclusive precision range check
        if ($inclusive) {
            return ($lengthToBeChecked >= $minimumPrecisionRange && $lengthToBeChecked <= $maximumPrecisionRange);
        }

        // Exclusive precision range check
        return ($lengthToBeChecked > $minimumPrecisionRange && $lengthToBeChecked < $maximumPrecisionRange);
    }


    /**
     * Count precision number
     *
     * @param float $value Value to be counted
     * @return int
     */
    public static function countNumberPrecision($value)
    {
        //$fNumber=$value;
        $fNumber = floatval($value);
        for ($iDecimals = 0; $fNumber != round($fNumber, $iDecimals); $iDecimals++);


        return $iDecimals;
    }

}

 