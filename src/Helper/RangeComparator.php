<?php  namespace Aedart\Validate\Helper; 

/**
 * Class Range Comparator
 *
 * Unit range comparator, which can validate if a given unit length is
 * within a specified range (inclusive or exclusive)
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Helper
 */
class RangeComparator {

    /**
     * Check if the given unit length is within the specified range
     *
     * @param int $lengthToBeChecked The unit length to be checked if it is within the specified range
     * @param int $minimumRange The minimum range
     * @param int $maximumRange The maximum range
     * @param bool $inclusive [Optional][Default true] If true, range is checked inclusively, false if range
     *                          is to be checked exclusively
     *
     * @return bool True if the given length is within the specified range, false if not
     */
    public static function isWithinRange($lengthToBeChecked, $minimumRange, $maximumRange, $inclusive = true){
        // Inclusive range check
        if($inclusive){
            return ($lengthToBeChecked >= $minimumRange && $lengthToBeChecked <= $maximumRange);
        }

        // Exclusive range check
        return ($lengthToBeChecked > $minimumRange && $lengthToBeChecked < $maximumRange);
    }

}