<?php  namespace Aedart\Validate\Date;

use Aedart\Validate\BaseValidator;
use Aedart\Validate\Exception\InvalidValidateOptionException;
use Aedart\Validate\Interfaces\Validator;
use DateTime;

/**
 * Class DateValidator
 *
 * An object validates date value
 *
 * @package Aedart\Validate\Date 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class DateValidator extends BaseValidator{

    /**
     * Validate the set validate-value, based upon eventual set validate-
     * options (if supported by concrete validator)
     *
     * @return bool True if the value is valid, false if not
     */
    public function validate() {
        $value = $this->getValidateValue();
        if($value instanceof DateTime){
            return true;
        }

        self::$lastErrorMessage = sprintf('"%s" is not a valid DateTime instance', var_export($value, true));

        return false;
    }
}

 