<?php  namespace Aedart\Validate\Number;

use Aedart\Validate\BaseValidator;
use Aedart\Validate\Helper\RangeComparator;
use Aedart\Validate\Interfaces\Options\InclusiveRangeOptionName;
use Aedart\Validate\Interfaces\Options\MaxRangeOptionName;
use Aedart\Validate\Interfaces\Options\MinRangeOptionName;
use Aedart\Validate\Traits\InclusiveRangeTrait;
use Aedart\Validate\Traits\MaxRangeTrait;
use Aedart\Validate\Traits\MinRangeTrait;

/**
 * Class Integer Validator
 *
 * <br />
 *
 * Validate if the given value is an integer
 *
 * <br />
 *
 * <b>Supported validation options</b>
 * <pre>
 *  $options = [
 *      IntegerValidator::MIN_RANGE          =>  -PHP_INT_MAX // Minimum range, default -PHP_INT_MAX
 *      IntegerValidator::MAX_RANGE          =>  PHP_INT_MAX // Maximum range, default PHP_INT_MAX
 *      IntegerValidator::INCLUSIVE_RANGE    =>  true // Inclusive range, default true
 *  ];
 * </pre>
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Number
 */
class IntegerValidator extends BaseValidator implements MinRangeOptionName, MaxRangeOptionName, InclusiveRangeOptionName{

    use MinRangeTrait,
        MaxRangeTrait,
        InclusiveRangeTrait;

    protected function getDefaultMinRange() {
        return -PHP_INT_MAX;
    }

    protected function getDefaultMaxRange() {
        return PHP_INT_MAX;
    }

    public function validate() {
        $value = $this->getValidateValue();

        if(!is_int($value)){
            self::$lastErrorMessage = sprintf('"%s" is not an integer', var_export($value, true));
            return false;
        }

        if(!RangeComparator::isWithinRange($value, $this->getMinRange(), $this->getMaxRange(), $this->isInclusiveRange())){
            self::$lastErrorMessage = sprintf('"%s" is out of range; minimum %d, maximum %d, inclusive %s', var_export($value, true), $this->getMinRange(), $this->getMaxRange(), $this->isInclusiveRange());
            return false;
        }

        return true;
    }

}