<?php  namespace Aedart\Validate\Number\Integer;

use Aedart\Validate\BaseValidator;
use Aedart\Validate\Helper\RangeComparator;
use Aedart\Validate\Interfaces\Options\InclusiveRangeOptionName;
use Aedart\Validate\Interfaces\Options\MaxRangeOptionName;
use Aedart\Validate\Traits\InclusiveRangeTrait;
use Aedart\Validate\Traits\MaxRangeTrait;
use Aedart\Validate\Number\IntegerValidator;

/**
 * Class Unsigned Integer Validator
 *
 * <br />
 *
 * Specialised integer validator, which has a default min and max range set
 *
 * <br />
 *
 * @see IntegerValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Number\Integer
 */
class UnsignedIntegerValidator extends IntegerValidator{

    protected function getDefaultMinRange() {
        return 0;
    }
}