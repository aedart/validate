<?php  namespace Aedart\Validate\Number\Integer;

use Aedart\Validate\Number\IntegerValidator;

/**
 * Class Signed Integer Validator
 *
 * Alias for IntegerValidator
 *
 * @see IntegerValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Number\Integer
 */
class SignedIntegerValidator extends IntegerValidator{

}