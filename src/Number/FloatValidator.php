<?php namespace Aedart\Validate\Number;

use Aedart\Validate\Helper\FloatPrecisionRangeComparator;
use Aedart\Validate\Interfaces\Options\InclusivePrecisionRangeOptionName;
use Aedart\Validate\Interfaces\Options\MaxPrecisionRangeOptionName;
use Aedart\Validate\Interfaces\Options\MinPrecisionRangeOptionName;
use Aedart\Validate\NumberValidator;
use Aedart\Validate\Traits\InclusivePrecisionRangeTrait;
use Aedart\Validate\Traits\MaxPrecisionRangeTrait;
use Aedart\Validate\Traits\MinPrecisionRangeTrait;

/**
 * Class FloatValidator
 *
 * Validate if the given value is a float
 *
 * <br />
 *
 * <b>Supported validation options</b>
 * <pre>
 *  $options = [
 *      FloatValidator::MIN_RANGE                   =>  -INF // Minimum range, default -INF
 *      FloatValidator::MAX_RANGE                   =>  INF // Maximum range, default INF
 *      FloatValidator::INCLUSIVE_RANGE             =>  true // Inclusive range, default true
 *      FloatValidator::MAX_PRECISION_RANGE         =>  FloatValidator::DEFAULT_MAX_PRECISION_RANGE,
 *      FloatValidator::MIN_PRECISION_RANGE         =>  FloatValidator::DEFAULT_MIN_PRECISION_RANGE,
 *      FloatValidator::INCLUSIVE_PRECISION_RANGE   =>  true // Inclusive precision range, default true
 *  ];
 * </pre>
 * @package Aedart\Validate\Number
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class FloatValidator extends NumberValidator implements MaxPrecisionRangeOptionName, MinPrecisionRangeOptionName, InclusivePrecisionRangeOptionName
{

    use MaxPrecisionRangeTrait,
            MinPrecisionRangeTrait,
            InclusivePrecisionRangeTrait;

    /**
     * Default minimum precision range value
     *
     * @var int
     */
    const DEFAULT_MIN_PRECISION_RANGE = 0;

    /**
     * Default maximum precision range value
     *
     * The size of a float is platform-dependent, although a maximum
     * of ~1.8e308 with a precision of roughly 14 decimal digits is
     * a common value (the 64 bit IEEE format)
     *
     * @see http://php.net/manual/en/language.types.float.php
     *
     *
     */
    const DEFAULT_MAX_PRECISION_RANGE = 14;


    /**
     * Get a default min precision range, if any is available
     *
     * @return int|null A default min precision range Or null if none available
     */
    protected function getDefaultMinPrecisionRange()
    {
        return self::DEFAULT_MIN_PRECISION_RANGE;
    }

    /**
     * Get a default max precision range, if any is available
     *
     * @return int|null A default max precision range Or null if none available
     */
    protected function getDefaultMaxPrecisionRange()
    {
        return self::DEFAULT_MAX_PRECISION_RANGE;
    }


    public function validate()
    {
        $value = $this->getValidateValue();

        if(!parent::validate()){
            return false;
        }

        if(!is_float($value)){
            self::$lastErrorMessage = sprintf('"%s" is not a float value', var_export($value, true));
            return false;
        }

        if(!FloatPrecisionRangeComparator::isWithinRange($value, $this->getMinPrecisionRange(), $this->getMaxPrecisionRange(), $this->isInclusivePrecisionRange())){
            self::$lastErrorMessage = sprintf('"%s" precision is out of range; minimum %d, maximum %d, inclusive %s',
                var_export($value, true),
                $this->getMinPrecisionRange(),
                $this->getMaxPrecisionRange(),
                $this->isInclusivePrecisionRange()
            );
            return false;
        }

        return true;
    }


}

 