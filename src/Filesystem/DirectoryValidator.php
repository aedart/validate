<?php  namespace Aedart\Validate\Filesystem;

use Aedart\Validate\BaseValidator;

/**
 * Class DirectoryValidator
 *
 * <br />
 *
 * Validate if the given value is valid filesystem directory path
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Filesystem
 */
class DirectoryValidator extends BaseValidator{

    /**
     * Validate the set validate-value, based upon eventual set validate-
     * options (if supported by concrete validator)
     *
     * @return bool True if the value is valid, false if not
     */
    public function validate() {
        $value = $this->getValidateValue();
        try {
            if(is_dir($value)){
                return true;
            }

            self::$lastErrorMessage = sprintf('"%s" is not a valid directory; it does not exist', var_export($value, true));
            return false;
        } catch(\Exception $e){
            self::$lastErrorMessage = sprintf('It appears that a is_dir validation could not be performed on "%s"', var_export($value, true));
            return false;
        }
    }
}