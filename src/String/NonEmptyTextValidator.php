<?php  namespace Aedart\Validate\String; 

use Aedart\Validate\String\TextValidator;

/**
 * Class Non-Empty Text Validator
 *
 * Specialised string text validator, that doesn't allow empty text;
 * with a minimum string length default set to 1
 *
 * @see TextValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\String
 */
class NonEmptyTextValidator extends TextValidator{

    protected function getDefaultMinRange() {
        return 1;
    }

}