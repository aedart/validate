<?php  namespace Aedart\Validate\String; 

use Aedart\Validate\StringValidator;

/**
 * Class Non Empty String Validator
 *
 * Specialised string validator, with a minimum string length default set to 1
 *
 * @see StringValidator
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\String
 */
class NonEmptyStringValidator extends StringValidator{

    protected function getDefaultMinRange() {
        return 1;
    }
}