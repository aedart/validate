<?php  namespace Aedart\Validate\String;

use Aedart\Validate\StringValidator;

/**
 * Class Text Validator
 *
 * <br />
 *
 * Specialised string validator, with a no maximum range set
 *
 * <br />
 *
 * <b>Supported validation options</b>
 * <pre>
 *  $options = [
 *      NumberValidator::MIN_RANGE          =>  0 // Minimum range, default 0
 *      NumberValidator::MAX_RANGE          =>  INF // Maximum range, default INF
 *      NumberValidator::INCLUSIVE_RANGE    =>  true // Inclusive range, default true
 *  ];
 * </pre>
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\String
 */
class TextValidator extends StringValidator{

    protected function getDefaultMaxRange() {
        return INF;
    }

    /**
     * <b>Override</b>
     *
     * Check if given value is a valid numeric value
     *
     * @param mixed $value The value to be checked
     *
     * @return bool True if value is a valid numeric value, false if not
     */
    protected function isMaxRangeValid($value) {
        return is_numeric($value);
    }
}