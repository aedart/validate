<?php  namespace Aedart\Validate;

use Aedart\Validate\BaseValidator;
use Aedart\Validate\Helper\RangeComparator;
use Aedart\Validate\Interfaces\Options\InclusiveRangeOptionName;
use Aedart\Validate\Interfaces\Options\MaxRangeOptionName;
use Aedart\Validate\Interfaces\Options\MinRangeOptionName;
use Aedart\Validate\Traits\InclusiveRangeTrait;
use Aedart\Validate\Traits\MaxRangeTrait;
use Aedart\Validate\Traits\MinRangeTrait;

/**
 * Class String Validator
 *
 * <br />
 *
 * Validate if the given value is a string
 *
 * <br />
 *
 * <b>Supported validation options</b>
 * <pre>
 *  $options = [
 *      StringValidator::MIN_RANGE          =>  0 // Minimum range, default 0
 *      StringValidator::MAX_RANGE          =>  255 // Maximum range, default 255
 *      StringValidator::INCLUSIVE_RANGE    =>  true // Inclusive range, default true
 *  ];
 * </pre>
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate
 */
class StringValidator extends BaseValidator implements MinRangeOptionName, MaxRangeOptionName, InclusiveRangeOptionName{

    use MinRangeTrait,
        MaxRangeTrait,
        InclusiveRangeTrait;

    protected function getDefaultMinRange() {
        return 0;
    }

    protected function getDefaultMaxRange() {
        return 255;
    }

    public function validate() {
        $value = $this->getValidateValue();
        if(!is_string($value)){
            self::$lastErrorMessage = sprintf('"%s" is not a string', var_export($value, true));
            return false;
        }

        if(!RangeComparator::isWithinRange(strlen($value), $this->getMinRange(), $this->getMaxRange(), $this->isInclusiveRange())){
            self::$lastErrorMessage = sprintf('"%s" is out of range; minimum %d, maximum %d, inclusive %s', var_export($value, true), $this->getMinRange(), $this->getMaxRange(), $this->isInclusiveRange());
            return false;
        }

        return true;
    }
}