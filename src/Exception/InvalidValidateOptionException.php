<?php  namespace Aedart\Validate\Exception; 

/**
 * Class Invalid Validate Option Exception
 *
 * Throw this exception if an unknown or invalid validate-option has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Exception
 */
class InvalidValidateOptionException extends \InvalidArgumentException{

}