<?php namespace Aedart\Validate\Interfaces;

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Interface Validator
 *
 * <br />
 *
 * When implementing this interface, a component promises that it can
 * perform a validation of some given value / data
 *
 * <br />
 *
 * This interface is inspired by PHP's native filter_var method as well as Zend Framework's
 * ValidatorInterface
 *
 * @link http://framework.zend.com
 * @link https://github.com/zendframework/zf2/blob/master/library/Zend/Validator/ValidatorInterface.php
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Interfaces
 */
interface Validator {

    /**
     * Get the last error message
     *
     * An error message is available, only if a given value has
     * not passed validation
     *
     * @return string The last error message if a given value was not valid or empty string if validation passed
     */
    public static function getLastErrorMessage();

    /**
     * Check if the given value is valid or not
     *
     * Invoking this method will reset any eventual previous error
     * messages.
     *
     * @see getLastErrorMessage()
     *
     * @param mixed $value The value to be validated
     * @param mixed[] $validateOptions [Optional] Eventual Key-value options array
     *
     * @return bool True if the given value is valid, false if not
     *
     * @throws InvalidValidateOptionException In case that a given validate-option is unknown or invalid
     */
    public static function isValid($value, array $validateOptions = []);

} 