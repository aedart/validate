<?php  namespace Aedart\Validate\Interfaces; 

/**
 * Interface ValidateValueAware
 *
 * Components that implement this interface, are aware of a value that
 * somehow must be validated. No validation is required for setting or
 * obtaining the given value - falls outside the scope of this
 * interface.
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Interfaces
 */
interface ValidateValueAware {

    /**
     * Set the value that must be validated
     *
     * @param mixed $value The value to be validated
     *
     * @return void
     */
    public function setValidateValue($value);

    /**
     * Get the value that must be validated
     *
     * @return mixed The value to be validated
     */
    public function getValidateValue();
}