<?php  namespace Aedart\Validate\Interfaces\Options; 

/**
 * Interface Inclusive Range Option Name
 *
 * Contains a single const; an option name
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Interfaces\Options
 */
interface InclusiveRangeOptionName {

    /**
     * Inclusive range - validate option name
     *
     * When provided, a given range is either inclusive
     * or exclusive
     */
    const INCLUSIVE_RANGE = 'inclusiveRange';

}