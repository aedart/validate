<?php namespace Aedart\Validate\Interfaces\Options;

/**
 * Interface InclusivePrecisionRangeOptionName
 *
 * Contains a single const; an option name
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 * @package Aedart\Validate\Interfaces\Options
 */
interface InclusivePrecisionRangeOptionName
{

    /**
     * Inclusive precision range - validate option name
     *
     * When provided, a given precision range is either inclusive
     * or exclusive
     */
    const INCLUSIVE_PRECISION_RANGE = 'inclusivePrecisionRange';

}