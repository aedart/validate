<?php  namespace Aedart\Validate\Interfaces\Options; 

/**
 * Interface Min Range Option Name
 *
 * Contains a single const; an option name
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Interfaces\Options
 */
interface MinRangeOptionName {

    /**
     * Minimum range - validate option name
     *
     * When provided, a given unit must have a minimum range
     * of the specified option value
     */
    const MIN_RANGE = 'minRange';

}