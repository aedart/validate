<?php  namespace Aedart\Validate\Interfaces\Options; 

/**
 * Interface Max Range Option Name
 *
 * Contains a single const; an option name
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Interfaces\Options
 */
interface MaxRangeOptionName {

    /**
     * Maximum range - validate option name
     *
     * When provided, a given unit must have a maximum range
     * of the specified option value
     */
    const MAX_RANGE = 'maxRange';

}