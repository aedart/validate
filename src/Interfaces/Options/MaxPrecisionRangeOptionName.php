<?php namespace Aedart\Validate\Interfaces\Options;
/**
 * Interface MaxPrecisionRangeOptionName
 *
 * Contains a single const; an option name
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Aedart\Validate\Interfaces\Options
 */
interface MaxPrecisionRangeOptionName
{

    /**
     * Maximum precision range - validate option name
     *
     * When provided, a given unit must have a maximum precision
     * range of the specified option value
     */
    const MAX_PRECISION_RANGE = 'maxPrecisionRange';

}