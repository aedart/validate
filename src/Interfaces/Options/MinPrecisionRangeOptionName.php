<?php namespace Aedart\Validate\Interfaces\Options;
/**
 * Interface MinPrecisionRangeOptionName
 *
 * Contains a single const; an option name
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Aedart\Validate\Interfaces\Options
 */
interface MinPrecisionRangeOptionName
{

    /**
     * Minimum precision range - validate option name
     *
     * When provided, a given unit must have a minimum precision
     * range of the specified option value
     */
    const MIN_PRECISION_RANGE = 'minPrecisionRange';

}