<?php  namespace Aedart\Validate;

use Aedart\Overload\Traits\PropertyOverloadTrait;
use Aedart\Validate\Exception\InvalidValidateOptionException;
use Aedart\Validate\Interfaces\ValidateValueAware;
use Aedart\Validate\Interfaces\Validator;
use Aedart\Util\Interfaces\Populatable;
use Aedart\Validate\Traits\ValidateValueTrait;

/**
 * Class Base Validator
 *
 * Abstraction that can be used as the base-class for validators.
 * Provides utility methods for dealing with eventual validation options,
 * by means of property-overloading
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate
 */
abstract class BaseValidator implements Validator,
    Populatable,
    ValidateValueAware
{

    use PropertyOverloadTrait,
        ValidateValueTrait;

    /**
     * Last error message
     *
     * @var string
     */
    protected static $lastErrorMessage = '';

    protected function __construct($value, array $validateOptions = []){
        $this->setValidateValue($value);
        $this->populate($validateOptions);
        self::$lastErrorMessage = '';
    }

    public function populate(array $data) {
        foreach($data as $key => $value){
            $this->__set($key, $value);
        }
    }

    /**
     * Validate the set validate-value, based upon eventual set validate-
     * options (if supported by concrete validator)
     *
     * @return bool True if the value is valid, false if not
     */
    abstract public function validate();

    public static function getLastErrorMessage(){
        return self::$lastErrorMessage;
    }

    public static function isValid($value, array $validateOptions = []) {
        try {
            $validator = new static($value, $validateOptions);
            return $validator->validate();
        } catch(\Exception $e){
            throw new InvalidValidateOptionException($e->getMessage(), 0, $e);
        }
    }

}