<?php namespace Aedart\Validate;
use Aedart\Validate\Exception\InvalidValidateOptionException;
use Aedart\Validate\Interfaces\Validator;

/**
 * Class AnythingValidator
 *
 * This validator is always correct,
 * Component does not care about variable,
 * returns always true
 *
 * @package Aedart\Validate
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class AnythingValidator implements Validator
{

    public static function isValid($value, array $validateOptions = [])
    {
        return true;
    }

    public static function getLastErrorMessage() {
        return '';
    }
}

 