<?php  namespace Aedart\Validate\Traits;

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Trait Min-Range
 *
 * Components that implement this trait, are able to specify
 * and retrieve a minimum range (of some kind of unit). Also,
 * a default may be specified
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Traits
 */
trait MinRangeTrait {

    /**
     * The minimum range of a value
     *
     * @var int|null
     */
    protected $minRange = null;

    /**
     * Set the min range
     *
     * @param int $value The minimum range
     *
     * @throws InvalidValidateOptionException In case that the range is invalid, not an integer!
     */
    protected function setMinRange($value){
        if(!$this->isMinRangeValid($value)){
            throw new InvalidValidateOptionException(sprintf('Min Range must be a valid integer, %s provided', var_export($value, true)));
        }
        $this->minRange = $value;
    }

    /**
     * Get the min range
     *
     * @return int|null Minimum range Or null of none specified and no default is available
     */
    protected function getMinRange(){
        if(!$this->hasMinRange() && $this->hasDefaultMinRange()){
            $this->setMinRange($this->getDefaultMinRange());
        }
        return $this->minRange;
    }

    /**
     * Check if a min range has been set
     *
     * @return bool True if a min range has been set, false if not
     */
    protected function hasMinRange(){
        if(!is_null($this->minRange)){
            return true;
        }
        return false;
    }

    /**
     * Get a default min range, if any is available
     *
     * @return int|null A default min range Or null if none available
     */
    protected function getDefaultMinRange(){
        return null;
    }

    /**
     * Check if a default min range is available
     *
     * @return bool True if a default min range is available, false if not
     */
    protected function hasDefaultMinRange(){
        if(!is_null($this->getDefaultMinRange())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given value is a valid integer
     *
     * @param mixed $value The value to be validated
     *
     * @return bool True if the value is an integer, false if not
     */
    protected function isMinRangeValid($value){
        return is_int($value);
    }
}