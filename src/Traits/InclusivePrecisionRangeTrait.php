<?php namespace Aedart\Validate\Traits;

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Trait InclusivePrecisionRangeTrait
 *
 * Components that implement this trait, are able to specify
 * if a given precision range is inclusive or exclusive. Furthermore,
 * a default can be specified
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 * @package Aedart\Validate\Traits
 */
trait InclusivePrecisionRangeTrait
{

    /**
     * The inclusive state of a number precision range
     *
     * @var bool|null
     */
    protected $inclusivePrecisionRange = null;

    /**
     * Set the inclusive precision range state
     *
     * @param bool $value True if precision range is inclusive, false if not
     *
     * @throws InvalidValidateOptionException In case that the inclusive state is not a boolean value
     */
    protected function setInclusivePrecisionRange($value)
    {
        if (!$this->isInclusivePrecisionRangeValid($value)) {
            throw new InvalidValidateOptionException(sprintf('Inclusive precision range must be a boolean, %s provided', var_export($value, true)));
        }
        $this->inclusivePrecisionRange = $value;
    }

    /**
     * Set the exclusive precision range state (inverse of inclusive)
     *
     * @param bool $value True if precision range is inclusive, false if not
     *
     * @throws InvalidValidateOptionException In case that the inclusive state is not a boolean value
     */
    protected function setExclusivePrecisionRange($value)
    {
        $this->setInclusivePrecisionRange(!$value);
    }

    /**
     * Get the inclusive precision range state
     *
     * @return bool True if precision range is inclusive, false if not
     */
    protected function getInclusivePrecisionRange()
    {
        if (!$this->hasInclusivePrecisionRange() && $this->hasDefaultInclusivePrecisionRange()) {
            $this->setInclusivePrecisionRange($this->getDefaultInclusivePrecisionRange());
        }
        return $this->inclusivePrecisionRange;
    }

    /**
     * Get the exclusive precision range state (inverse of inclusive precision range)
     *
     * @return bool True if precision range is exclusive, false if not
     */
    protected function getExclusivePrecisionRange()
    {
        return !$this->getInclusivePrecisionRange();
    }

    /**
     * Alias for getInclusivePrecisionRange
     *
     * @return bool True if precision range is inclusive, false if not
     */
    protected function isInclusivePrecisionRange()
    {
        return $this->getInclusivePrecisionRange();
    }

    /**
     * Check if precision range state is exclusive (inverse of inclusive)
     *
     * @return bool True if precision range is exclusive, false if not
     */
    protected function isExclusive()
    {
        return $this->getExclusivePrecisionRange();
    }

    /**
     * Check if a inclusive precision range state has been set
     *
     * @return bool True if a inclusive precision range state has been set, false if not
     */
    protected function hasInclusivePrecisionRange()
    {
        if (!is_null($this->inclusivePrecisionRange)) {
            return true;
        }
        return false;
    }

    /**
     * Get a default inclusive precision range state, if any is available
     *
     * @return bool|null A default inclusive precision range state Or null if none available
     */
    protected function getDefaultInclusivePrecisionRange()
    {
        return true;
    }

    /**
     * Check if a default inclusive range state is available
     *
     * @return bool True if a default inclusive range state is available, false if not
     */
    protected function hasDefaultInclusivePrecisionRange()
    {
        if (!is_null($this->getDefaultInclusivePrecisionRange())) {
            return true;
        }
        return false;
    }

    /**
     * Check if the given value is a valid boolean
     *
     * @param mixed $value The value to be validated
     *
     * @return bool True if the value is an boolean, false if not
     */
    protected function isInclusivePrecisionRangeValid($value)
    {
        return is_bool($value);
    }
}