<?php namespace Aedart\Validate\Traits;

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Trait MinPrecisionRangeTrait
 *
 * Components that implement this trait, are able to specify
 * and retrieve a minimum precision range. Also,
 * a default may be specified
 *
 * @package      Aedart\Validate\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
trait MinPrecisionRangeTrait
{


    /**
     * The minimum precision range
     *
     * @var null|int
     */
    protected $minPrecisionRange = null;


    /**
     * Set the min precision range
     *
     * @param int $value The minimum precision range
     *
     * @throws InvalidValidateOptionException In case that the precision is invalid, not an integer!
     */
    protected function setMinPrecisionRange($value)
    {
        if (!$this->isMinPrecisionRangeValid($value)) {
            throw new InvalidValidateOptionException(sprintf('Min precision range must be a valid integer, %s provided', var_export($value, true)));
        }
        $this->minPrecisionRange = $value;
    }

    /**
     * Get the min precision range
     *
     * @return int|null Minimum precision range Or null of none specified and no default is available
     */
    protected function getMinPrecisionRange()
    {
        if (!$this->hasMinPrecisionRange() && $this->hasDefaultMinPrecisionRange()) {
            $this->setMinPrecisionRange($this->getDefaultMinPrecisionRange());
        }
        return $this->minPrecisionRange;
    }

    /**
     * Check if a min precision range has been set
     *
     * @return bool True if a min precision range has been set, false if not
     */
    protected function hasMinPrecisionRange()
    {
        if (!is_null($this->minPrecisionRange)) {
            return true;
        }
        return false;
    }

    /**
     * Get a default min precision range, if any is available
     *
     * @return int|null A default min precision range Or null if none available
     */
    protected function getDefaultMinPrecisionRange()
    {
        return null;
    }

    /**
     * Check if a default min precision range is available
     *
     * @return bool True if a default min precision range is available, false if not
     */
    protected function hasDefaultMinPrecisionRange()
    {
        if (!is_null($this->getDefaultMinPrecisionRange())) {
            return true;
        }
        return false;
    }

    /**
     * Check if the given value is a valid integer
     *
     * @param mixed $value The value to be validated
     *
     * @return bool True if the value is an integer, false if not
     */
    protected function isMinPrecisionRangeValid($value)
    {
        return is_int($value) && $value >= 0;
    }
}