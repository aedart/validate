<?php  namespace Aedart\Validate\Traits; 

/**
 * Trait Validate Value
 *
 * @see ValidateValueAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Traits
 */
trait ValidateValueTrait {

    /**
     * The value to be validated
     *
     * @var mixed
     */
    protected $validateValue = null;

    /**
     * Set the value that must be validated
     *
     * @param mixed $value The value to be validated
     *
     * @return void
     */
    public function setValidateValue($value){
        $this->validateValue = $value;
    }

    /**
     * Get the value that must be validated
     *
     * @return mixed The value to be validated
     */
    public function getValidateValue(){
        return $this->validateValue;
    }

}