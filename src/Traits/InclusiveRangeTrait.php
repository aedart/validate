<?php  namespace Aedart\Validate\Traits; 

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Trait Inclusive-Range
 *
 * Components that implement this trait, are able to specify
 * if a given range is inclusive or exclusive. Furthermore,
 * a default can be specified
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Traits
 */
trait InclusiveRangeTrait {

    /**
     * The inclusive state of a range
     *
     * @var bool|null
     */
    protected $inclusiveRange = null;

    /**
     * Set the inclusive range state
     *
     * @param bool $value True if range is inclusive, false if not
     *
     * @throws InvalidValidateOptionException In case that the inclusive state is not a boolean value
     */
    protected function setInclusiveRange($value){
        if(!$this->isInclusiveRangeValid($value)){
            throw new InvalidValidateOptionException(sprintf('Inclusive range must be a boolean, %s provided', var_export($value, true)));
        }
        $this->inclusiveRange = $value;
    }

    /**
     * Set the exclusive range state (inverse of inclusive)
     *
     * @param bool $value True if range is inclusive, false if not
     *
     * @throws InvalidValidateOptionException In case that the inclusive state is not a boolean value
     */
    protected function setExclusiveRange($value){
        $this->setInclusiveRange(!$value);
    }

    /**
     * Get the inclusive range state
     *
     * @return bool True if range is inclusive, false if not
     */
    protected function getInclusiveRange(){
        if(!$this->hasInclusiveRange() && $this->hasDefaultInclusiveRange()){
            $this->setInclusiveRange($this->getDefaultInclusiveRange());
        }
        return $this->inclusiveRange;
    }

    /**
     * Get the exclusive range state (inverse of inclusive range)
     *
     * @return bool True if range is exclusive, false if not
     */
    protected function getExclusiveRange(){
        return !$this->getInclusiveRange();
    }

    /**
     * Alias for getInclusiveRange
     *
     * @return bool True if range is inclusive, false if not
     */
    protected function isInclusiveRange(){
        return $this->getInclusiveRange();
    }

    /**
     * Check if range state is exclusive (inverse of inclusive)
     *
     * @return bool True if range is exclusive, false if not
     */
    protected function isExclusive(){
        return $this->getExclusiveRange();
    }

    /**
     * Check if a inclusive range state has been set
     *
     * @return bool True if a inclusive range state has been set, false if not
     */
    protected function hasInclusiveRange(){
        if(!is_null($this->inclusiveRange)){
            return true;
        }
        return false;
    }

    /**
     * Get a default inclusive range state, if any is available
     *
     * @return bool|null A default inclusive range state Or null if none available
     */
    protected function getDefaultInclusiveRange(){
        return true;
    }

    /**
     * Check if a default inclusive range state is available
     *
     * @return bool True if a default inclusive range state is available, false if not
     */
    protected function hasDefaultInclusiveRange(){
        if(!is_null($this->getDefaultInclusiveRange())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given value is a valid boolean
     *
     * @param mixed $value The value to be validated
     *
     * @return bool True if the value is an boolean, false if not
     */
    protected function isInclusiveRangeValid($value){
        return is_bool($value);
    }
}