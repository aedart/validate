<?php  namespace Aedart\Validate\Traits; 

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Trait Max-Range
 *
 * Components that implement this trait, are able to specify
 * and retrieve a maximum range (of some kind of unit). Also,
 * a default may be specified
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate\Traits
 */
trait MaxRangeTrait {

    /**
     * The minimum range of a value
     *
     * @var int|null
     */
    protected $maxRange = null;

    /**
     * Set the max range
     *
     * @param int $value The maximum range
     *
     * @throws InvalidValidateOptionException In case that the range is invalid, not an integer!
     */
    protected function setMaxRange($value){
        if(!$this->isMaxRangeValid($value)){
            throw new InvalidValidateOptionException(sprintf('Max Range must be a valid integer, %s provided', var_export($value, true)));
        }
        $this->maxRange = $value;
    }

    /**
     * Get the max range
     *
     * @return int|null Minimum range Or null of none specified and no default is available
     */
    protected function getMaxRange(){
        if(!$this->hasMaxRange() && $this->hasDefaultMaxRange()){
            $this->setMaxRange($this->getDefaultMaxRange());
        }
        return $this->maxRange;
    }

    /**
     * Check if a max range has been set
     *
     * @return bool True if a max range has been set, false if not
     */
    protected function hasMaxRange(){
        if(!is_null($this->maxRange)){
            return true;
        }
        return false;
    }

    /**
     * Get a default max range, if any is available
     *
     * @return int|null A default max range Or null if none available
     */
    protected function getDefaultMaxRange(){
        return null;
    }

    /**
     * Check if a default max range is available
     *
     * @return bool True if a default max range is available, false if not
     */
    protected function hasDefaultMaxRange(){
        if(!is_null($this->getDefaultMaxRange())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given value is a valid integer
     *
     * @param mixed $value The value to be validated
     *
     * @return bool True if the value is an integer, false if not
     */
    protected function isMaxRangeValid($value){
        return is_int($value);
    }
}