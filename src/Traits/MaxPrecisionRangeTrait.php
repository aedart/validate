<?php namespace Aedart\Validate\Traits;

use Aedart\Validate\Exception\InvalidValidateOptionException;

/**
 * Trait MaxPrecisionRangeTrait
 *
 * Components that implement this trait, are able to specify
 * and retrieve a maximum precision range. Also,
 * a default may be specified
 *
 * @package      Aedart\Validate\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
trait MaxPrecisionRangeTrait
{


    /**
     * The maximum precision range
     *
     * @var null|int
     */
    protected $maxPrecisionRange = null;


    /**
     * Set the max precision range
     *
     * @param int $value The maximum range precision
     *
     * @throws InvalidValidateOptionException In case that the precision is invalid, not an integer!
     */
    protected function setMaxPrecisionRange($value)
    {
        if (!$this->isMaxPrecisionRangeValid($value)) {
            throw new InvalidValidateOptionException(sprintf('Max precision range must be a valid integer, %s provided', var_export($value, true)));
        }
        $this->maxPrecisionRange = $value;
    }

    /**
     * Get the max precision range
     *
     * @return int|null Maximum precision range Or null of none specified and no default is available
     */
    protected function getMaxPrecisionRange()
    {
        if (!$this->hasMaxPrecisionRange() && $this->hasDefaultMaxPrecisionRange()) {
            $this->setMaxPrecisionRange($this->getDefaultMaxPrecisionRange());
        }
        return $this->maxPrecisionRange;
    }

    /**
     * Check if a max precision range has been set
     *
     * @return bool True if a max precision range has been set, false if not
     */
    protected function hasMaxPrecisionRange()
    {
        if (!is_null($this->maxPrecisionRange)) {
            return true;
        }
        return false;
    }

    /**
     * Get a default max precision range, if any is available
     *
     * @return int|null A default max precision range Or null if none available
     */
    protected function getDefaultMaxPrecisionRange()
    {
        return null;
    }

    /**
     * Check if a default max precision range is available
     *
     * @return bool True if a default max precision range is available, false if not
     */
    protected function hasDefaultMaxPrecisionRange()
    {
        if (!is_null($this->getDefaultMaxPrecisionRange())) {
            return true;
        }
        return false;
    }

    /**
     * Check if the given value is a valid integer
     *
     * @param mixed $value The value to be validated
     *
     * @return bool True if the value is an integer, false if not
     */
    protected function isMaxPrecisionRangeValid($value)
    {
        return is_int($value) && $value >= 0;
    }
}