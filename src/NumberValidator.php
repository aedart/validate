<?php  namespace Aedart\Validate;

use Aedart\Validate\BaseValidator;
use Aedart\Validate\Helper\RangeComparator;
use Aedart\Validate\Interfaces\Options\InclusiveRangeOptionName;
use Aedart\Validate\Interfaces\Options\MaxRangeOptionName;
use Aedart\Validate\Interfaces\Options\MinRangeOptionName;
use Aedart\Validate\Traits\InclusiveRangeTrait;
use Aedart\Validate\Traits\MaxRangeTrait;
use Aedart\Validate\Traits\MinRangeTrait;

/**
 * Class Number Validator
 *
 * <br />
 *
 * Validate if the given value is a number or a numeric string.
 *
 * <br />
 *
 * <b>Supported validation options</b>
 * <pre>
 *  $options = [
 *      NumberValidator::MIN_RANGE          =>  -INF // Minimum range, default -INF
 *      NumberValidator::MAX_RANGE          =>  INF // Maximum range, default INF
 *      NumberValidator::INCLUSIVE_RANGE    =>  true // Inclusive range, default true
 *  ];
 * </pre>
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Validate
 */
class NumberValidator extends BaseValidator implements MinRangeOptionName, MaxRangeOptionName, InclusiveRangeOptionName{

    use MinRangeTrait,
        MaxRangeTrait,
        InclusiveRangeTrait;

    protected function getDefaultMinRange() {
        return -INF;
    }

    protected function getDefaultMaxRange() {
        return INF;
    }

    /**
     * <b>Override</b>
     *
     * Check if given value is a valid numeric value
     *
     * @param mixed $value The value to be checked
     *
     * @return bool True if value is a valid numeric value, false if not
     */
    protected function isMinRangeValid($value) {
        return is_numeric($value);
    }

    /**
     * <b>Override</b>
     *
     * Check if given value is a valid numeric value
     *
     * @param mixed $value The value to be checked
     *
     * @return bool True if value is a valid numeric value, false if not
     */
    protected function isMaxRangeValid($value) {
        return is_numeric($value);
    }

    public function validate() {
        $value = $this->getValidateValue();

        if(!is_numeric($value)){
            self::$lastErrorMessage = sprintf('"%s" is not numeric', var_export($value, true));
            return false;
        }

        if(!RangeComparator::isWithinRange($value, $this->getMinRange(), $this->getMaxRange(), $this->isInclusiveRange())){
            self::$lastErrorMessage = sprintf('"%s" is out of range; minimum %d, maximum %d, inclusive %s', var_export($value, true), $this->getMinRange(), $this->getMaxRange(), $this->isInclusiveRange());
            return false;
        }

        return true;
    }
}