## Validate ##

**No longer maintained**

A validate package which allows to perform various sorts of simple validation upon primates as well as defining how validators should behave

## Contents ##

[TOC]

## When to use this ##

When you need to perform a simple check if something is valid or not

## How to install ##

```
#!console

composer require aedart/validate 0.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Acknowledgement ##

The core interface for the validators is greatly inspired by [Zend Framework's](http://framework.zend.com) [ValidatorInterface](https://github.com/zendframework/zf2/blob/master/library/Zend/Validator/ValidatorInterface.php)

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package